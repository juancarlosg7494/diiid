﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookSocial.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Autor { get; set; }
        public string Caratula { get; set; }


        public static List<Book> GetBooks()
        {
            return new List<Book>()
            {
                new Book { Id= 1, Nombre = "El Gato Negro", Autor = "Edgar Allan Poe", Caratula = "https://images.cdn2.buscalibre.com/fit-in/360x360/40/6d/406da4c310781b0cff3cc79d34eebf67.jpg" } ,
                new Book { Id= 2, Nombre = "El Túnel", Autor = "Ernesto Sabato", Caratula = "https://images.cdn2.buscalibre.com/fit-in/360x360/40/6d/406da4c310781b0cff3cc79d34eebf67.jpg" },
                new Book { Id= 3, Nombre = "IT, El resplandor", Autor = "Stephen King", Caratula = "https://images.cdn2.buscalibre.com/fit-in/360x360/40/6d/406da4c310781b0cff3cc79d34eebf67.jpg" },
                new Book { Id= 4, Nombre = "Farenhet 451", Autor = "Ray Bradbury", Caratula = "https://images.cdn2.buscalibre.com/fit-in/360x360/40/6d/406da4c310781b0cff3cc79d34eebf67.jpg" },
                new Book { Id= 5, Nombre = "La Iliada", Autor = "Homero", Caratula = "https://images.cdn2.buscalibre.com/fit-in/360x360/40/6d/406da4c310781b0cff3cc79d34eebf67.jpg" },
                new Book { Id= 6, Nombre = "Metro 2033", Autor = "Dmitri Glujovski", Caratula = "https://images.cdn2.buscalibre.com/fit-in/360x360/40/6d/406da4c310781b0cff3cc79d34eebf67.jpg" },
                new Book { Id= 7, Nombre = "El extraño caso del Dr. Jeckil y Mr Hide", Autor = "Robert Louis Stevenson", Caratula = "https://images.cdn2.buscalibre.com/fit-in/360x360/40/6d/406da4c310781b0cff3cc79d34eebf67.jpg" },
                new Book { Id= 8, Nombre = "Sangre de Campeón", Autor = "Carlos Cuahtemoc Sanchez", Caratula = "https://images.cdn2.buscalibre.com/fit-in/360x360/40/6d/406da4c310781b0cff3cc79d34eebf67.jpg" },
                new Book { Id= 9, Nombre = "La Serpiente de Oro", Autor = "Ciro Alegría", Caratula = "https://images.cdn2.buscalibre.com/fit-in/360x360/40/6d/406da4c310781b0cff3cc79d34eebf67.jpg" },
            };
        }
    }
}

