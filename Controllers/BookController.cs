﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookSocial.Models;
using Microsoft.AspNetCore.Mvc;

namespace BookSocial.Controllers
{
    public class BookController : Controller
    {
        [HttpGet]
        public ViewResult Detalle(int id)
        {

            var books = Book.GetBooks();
            //Linq metodos para listas
            Book book = books.FirstOrDefault(item => item.Id == id); // el primero que encuentre o null
            // item es un nombre puede ser cualquier cosa

            //foreach (var item in books)
            //{
            //    if (item.Id == id)
            //        book = item;
            //}

            return View(book);
        }


        public string Comentario(int id, string detalle)
        {
            return detalle;
        }
    }
}

