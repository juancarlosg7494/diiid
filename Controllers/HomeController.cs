﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BookSocial.Models;

namespace BookSocial.Controllers
{
    public class HomeController : Controller
    {
        public 
        public IActionResult Index()
        {
            var books = Book.GetBooks();
            return View("Index", books);
        }
    }
}

